/* eslint-disable linebreak-style */
import React from 'react';
import { View } from 'react-native';

const Gap = ({ width, height }) => (
  <View style={{ height, width }} />
);

export default Gap;
