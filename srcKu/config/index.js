/* eslint-disable linebreak-style */
const APP_NAME = 'Indoor Air Quality';
const APP_VERSION = '1.0';
const API_BASE_URL = 'https://reqres.in/api/';

export {
  APP_NAME,
  APP_VERSION,
  API_BASE_URL,
};
